//node.js : 자바같은 서버용 언어

//npm : 추가기능 다운로드 할수 있는것

// vuejs nuxtjs

//npm npx 를 이용해서 nuxt 설치하는법
// npm init nuxt-app@latest sample_project

// y

//npx execute
//npm manager

// npm install

//devDependencies 종속성
// ^  ^의 의미 : ~이상
// npm run dev

// 웹스톰 run/debug configurations 들어가서 npm : run : dev (scripts)입력

// vue 는 app.vue

//vuejs -> 자바스크립트의 프레임워크 (기본 프레임워크는 쓰기 불편함)
//nuxtjs -> vue.js의 프레임워크

//vue 문법을 공부해야함
//빠르게 공부하는법 언어의 차이점만 보기.
//^ vue 문법은 캡틴판교꺼보기 한번만 읽기.

//*
//vue의 중요한 컴포넌트 배우기
//컴포넌트 : DB / 컴포넌트
//뷰에서 만드는 모든 파일은 컴포넌트 (.vue)

//네이버에서 컴포넌트인부분 찾기

//SPA상에서
//헤더와 푸터 = 레이아웃 컴포넌트
//메인 = 페이지 컴포넌트
//그안에 여러 컴포넌트는 일반 컴포넌트.

//네이버 컴포넌트 따라해보기

//& 줄단위로 컴포넌트를 제작시 : 레이아웃이 달라질,추가될 경우 대응이 안됨.
// i.e ) 컴포넌트의 단위는 레이아웃 구성 중 최소단위로 제작한다.

//nuxt.config.ts/js 설정
//  components path : 경로
// extensions : 어떤확장자

// 경로상의 ~뜻 : 프로젝트 절대경로
// components 폴더가없으면 폴더경로만들어주기
// 네이버상의 뉴스스탠드 컴포넌트는 : 마이크로 컴포넌트라고 함
//마이크로 컴포넌트는 다른 페이지에서도 사용 할 수 있음

//& 컴포넌트의 핵심은 "재사용"

// components 폴더 안에 micro 폴더 생성

// 안쪽 파일은 파스칼 케이스